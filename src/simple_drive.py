#! /usr/bin/env python

import rospy
import serial
import argparse
from JrkG2Serial import JrkG2Serial
from geometry_msgs.msg import Twist
from numpy import interp


class JrkG2Drive(object):
    def __init__(self, lport, rport, baud):
        self.lport = self.get_jrk_port(lport, baud)
        self.rport = self.get_jrk_port(rport, baud)

        rospy.init_node("simple_drive")    
        self.subscriber_twist = rospy.Subscriber("cmd_vel", Twist, self.on_new_twist, queue_size=10)    
        rospy.spin()


    def on_new_twist(self, twist):    
        #rospy.loginfo("Twist: linear.x={}, angular.z={}".format(twist.linear.x, twist.angular.z))
        left_speed = (twist.linear.x - twist.angular.z) * 1000.0
        right_speed = (twist.linear.x + twist.angular.z) * 1000.0
        #rospy.loginfo("Speed: Left={}, Right={}".format(left_speed, right_speed))
        left_target = left_speed + 2048
        right_target = right_speed + 2048
        #left_target = interp(left_speed,[-100,100],[-3000,3000])
        #right_target = interp(right_speed,[-100,100],[-3000,3000])
        rospy.loginfo("Speed: Left={}, Right={}".format(left_target, right_target))
        self.lport.set_target(int(left_target))
        self.rport.set_target(int(right_target))


    def get_jrk_port(self, port_name, baud_rate):
        port = serial.Serial(port_name, baud_rate, timeout=0.1, write_timeout=0.1)    
        return JrkG2Serial(port, None)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()    
    #parser.add_argument('ports', action='append', help='Serial port(s) to monitor.', required=True)
    parser.add_argument('lport', help='Serial port to left wheel.')
    parser.add_argument('rport', help='Serial port to right wheel.')    
    parser.add_argument('-b','--baud', type=int , help='Serial port baud rate. Default is 9600.', default=9600)    
    args, unknown = parser.parse_known_args()

    try:
        driver = JrkG2Drive(args.lport, args.rport, args.baud)
    except rospy.ROSInterruptException:
        pass
    finally:
        rospy.loginfo("simple_drive node stopped.")