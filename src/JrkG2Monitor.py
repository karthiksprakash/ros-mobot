#! /usr/bin/env python

import serial
import JrkG2Serial
import argparse
import rospy
from mobot.msg import motor_status


def monitor(args):            
    rospy.init_node('~{}_monitor'.format(args.name))
    pub = rospy.Publisher('~{}_status'.format(args.name), motor_status, queue_size=10)
    
    jrk = get_jrk_port(args.port, args.baud)
    rospy.loginfo("JRK port is {}".format(args.port))

    rate = rospy.Rate(args.rate) 
    rospy.loginfo("Publish rate is {} hz".format(args.rate))

    while not rospy.is_shutdown():
        msg = get_jrk_status(jrk)
        pub.publish(msg)
        rate.sleep()


def get_jrk_status(jrk):
    status = motor_status()
    status.vin = jrk.get_vin()
    status.current = jrk.get_current()
    status.feedback = jrk.get_feedback()
    return status


def get_jrk_port(port_name, baud_rate):
    port = serial.Serial(port_name, baud_rate, timeout=0.1, write_timeout=0.1)    
    return JrkG2Serial.JrkG2Serial(port, None)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()    
    #parser.add_argument('ports', action='append', help='Serial port(s) to monitor.', required=True)
    parser.add_argument('port', help='Serial port to monitor.')
    parser.add_argument('name', help='Name for the wheel/motor.')
    parser.add_argument('-b','--baud', type=int , help='Serial port baud rate. Default is 9600.', default=9600)
    parser.add_argument('-r','--rate', type=int , help='Publish rate in hz. Default is 2 hz.', default=2)    
    args, unknown = parser.parse_known_args()

    try:
        monitor(args)
    except rospy.ROSInterruptException:
        pass
    finally:
        rospy.loginfo("{} node stopped.".format(args.name))

